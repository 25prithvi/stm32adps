/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f4xx_it.h
  * @brief   This file contains the headers of the interrupt handlers.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
 ******************************************************************************
  */
/* USER CODE END Header */
#ifndef __ADPS_SCREEN_H
#define __ADPS_SCREEN_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

extern enum {
	GOTO_MAIN_SCREEN,
	UPDATE_MAIN_SCREEN,
	SETTINGS_SCREEN,
	EDIT_WATER_METER_SCREEN,
	EDIT_AUTOBLOWDOWN_SCREEN,
	EDIT_CHEMICAL1_SCREEN,
	EDIT_CHEMICAL2_SCREEN,
	QR_SCREEN,
}SCREEN_STATE;

extern int current_screen_state;
extern unsigned char end_char[4];
extern int settings_changed_flag;

/* Exported functions prototypes ---------------------------------------------*/

/* USER CODE BEGIN EFP */
void screen_actions(void);
void update_mainscreen(void);
void get_settings(char *request);
/* USER CODE END EFP */

#ifdef __cplusplus
}
#endif

#endif /* __ADPS_SCREEN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
