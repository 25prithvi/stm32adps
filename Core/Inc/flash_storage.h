/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f4xx_it.h
  * @brief   This file contains the headers of the interrupt handlers.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
 ******************************************************************************
  */
/* USER CODE END Header */
#ifndef __FLASH_STORAGE_H
#define __FLASH_STORAGE_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
#define water_meter_setting_address 					0x081E0000
#define chemical1_settings_setting_address 				0x081E0004
#define chemical2_settings_setting_address 				0x081E0008
#define auto_blowdown_function_settings_setting_address 0x081E0012


/* USER CODE END ET */
void get_settings_flash(settings *flash_water_settings);
void set_settings_flash(settings *flash_water_settings);
/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
static void FLASH_Program_Word(uint32_t Address, uint32_t Data);
static uint32_t FLASH_READ(uint32_t Flash_address);
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */




/* Exported functions prototypes ---------------------------------------------*/


/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

#ifdef __cplusplus
}
#endif

#endif /* __FLASH_STORAGE_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
