/* USER CODE BEGIN Header */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "keys.h"
#include "adps_screen.h"
#include "keypad_screen.h"
#include "flash_storage.h"

UART_HandleTypeDef huart5;

TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;

char data[50];
char total_water_req[40];
char ph_req[20];
char tds_req[20];
char chemical1_req[30];
char chemical2_req[30];
unsigned char end_char[4] = {0xff,0xff,0xff,'\0'};


char water_meter_req[40];

int current_screen_state = GOTO_MAIN_SCREEN;
int settings_changed_flag = 0;

void screen_actions(){

	switch (current_screen_state){

	case GOTO_MAIN_SCREEN:
		sprintf(data,"page 2%s\0",end_char);
		HAL_UART_Transmit_IT(&huart5,data,strlen(data));
		current_screen_state = UPDATE_MAIN_SCREEN;
		break;

	case UPDATE_MAIN_SCREEN:
		if(key_pressed == NONE_KEY_PRESSED)
			update_mainscreen();

		else if(key_pressed == LEFT_KEY_PRESSED)
		{
			sprintf(data,"page 4%s\0",end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = QR_SCREEN;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			sprintf(data,"page 3%s\0",end_char);
			//HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			get_settings(&data);
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = SETTINGS_SCREEN;
		}
		break;

	case SETTINGS_SCREEN:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			sprintf(data,"page 2%s\0",end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = UPDATE_MAIN_SCREEN;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			sprintf(data,"page 4%s\0",end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = QR_SCREEN;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			sprintf(data,"t2.bco=0%st2.pco=65535%s\0",end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = EDIT_WATER_METER_SCREEN;
		}

		if(settings_changed_flag == 1)
		{
			settings_changed_flag = 2;
		}
		break;

	case EDIT_WATER_METER_SCREEN:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			sprintf(data,"t2.bco=65535%st2.pco=0%st8.bco=0%st8.pco=65535%s\0",end_char,end_char,end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = EDIT_CHEMICAL2_SCREEN;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			sprintf(data,"t2.bco=65535%st2.pco=0%sb0.bco=0%sb0.bco2=0%s\0",end_char,end_char,end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = EDIT_AUTOBLOWDOWN_SCREEN;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			sprintf(data,"t2.bco=65535%st2.pco=0%s\0",end_char,end_char);
			//HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			get_settings(&data);
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = SETTINGS_SCREEN;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			sprintf(data,"click t2,0%sb1.bco=0%s\0",end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			sprintf(edit_data,"%d",water_settings.water_meter_settings);
			edit_data_len = strlen(edit_data);
			key_pressed = NONE_KEY_PRESSED;
			HAL_TIM_Base_Start_IT(&htim5);
			HAL_TIM_Base_Stop_IT(&htim4);
			//current_screen_state = SETTINGS_SCREEN;
		}
		break;

	case EDIT_AUTOBLOWDOWN_SCREEN:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			sprintf(data,"t2.bco=0%st2.pco=65535%sb0.bco=65535%sb0.bco2=65535%s\0",end_char,end_char,end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = EDIT_WATER_METER_SCREEN;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			sprintf(data,"t5.bco=0%st5.pco=65535%sb0.bco=65535%sb0.bco2=65535%s\0",end_char,end_char,end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = EDIT_CHEMICAL1_SCREEN;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			sprintf(data,"b0.bco=65535%sb0.pco=0%s\0",end_char,end_char);
			//HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			get_settings(&data);
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = SETTINGS_SCREEN;
		}
		break;

	case EDIT_CHEMICAL1_SCREEN:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			sprintf(data,"t5.bco=65535%st5.pco=0%sb0.bco=0%sb0.bco2=0%s\0",end_char,end_char,end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = EDIT_AUTOBLOWDOWN_SCREEN;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			sprintf(data,"t5.bco=65535%st5.pco=0%st8.bco=0%st8.pco=65535%s\0",end_char,end_char,end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = EDIT_CHEMICAL2_SCREEN;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			sprintf(data,"t5.bco=65535%st5.pco=0%s\0",end_char,end_char);
			//HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			get_settings(&data);
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = SETTINGS_SCREEN;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			sprintf(data,"click t5,0%sb1.bco=0%s\0",end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			sprintf(edit_data,"%.2f",water_settings.chemical1_settings);
			edit_data_len = strlen(edit_data);
			key_pressed = NONE_KEY_PRESSED;
			HAL_TIM_Base_Start_IT(&htim5);
			HAL_TIM_Base_Stop_IT(&htim4);
			//current_screen_state = SETTINGS_SCREEN;
		}
		break;

	case EDIT_CHEMICAL2_SCREEN:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			sprintf(data,"t5.bco=0%st5.pco=65535%st8.bco=65535%st8.pco=0%s\0",end_char,end_char,end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = EDIT_CHEMICAL1_SCREEN;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			sprintf(data,"t2.bco=0%st2.pco=65535%st8.bco=65535%st8.pco=0%s\0",end_char,end_char,end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = EDIT_WATER_METER_SCREEN;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			sprintf(data,"t8.bco=65535%st8.pco=0%s\0",end_char,end_char);
			//HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			get_settings(&data);
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = SETTINGS_SCREEN;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			sprintf(data,"click t8,0%sb1.bco=0%s\0",end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			sprintf(edit_data,"%.2f",water_settings.chemical2_settings);
			edit_data_len = strlen(edit_data);
			key_pressed = NONE_KEY_PRESSED;
			HAL_TIM_Base_Start_IT(&htim5);
			HAL_TIM_Base_Stop_IT(&htim4);
			//current_screen_state = SETTINGS_SCREEN;
		}
		break;

	case QR_SCREEN:
		if(key_pressed == NONE_KEY_PRESSED){
			char data[150];
			sprintf(data,"qr0.txt=\"%u,%.2f,%.2f,%.2f,%.2f\"%s\0",total_water,ph,tds,chemical1,chemical2,end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));

		}
		else if(key_pressed == LEFT_KEY_PRESSED)
		{
			sprintf(data,"page 3%s\0",end_char);
			//HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			get_settings(&data);
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = SETTINGS_SCREEN;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			sprintf(data,"page 2%s\0",end_char);
			HAL_UART_Transmit_IT(&huart5,data,strlen(data));
			key_pressed = NONE_KEY_PRESSED;
			current_screen_state = UPDATE_MAIN_SCREEN;
		}
		break;

	}
}



void update_mainscreen(){
	char data[150];

	sprintf(total_water_req,"t%d.txt=\"%u\"%s\0",1,total_water,end_char);
	sprintf(ph_req,"t%d.txt=\"%.2f\"%s\0",4,ph,end_char);
	sprintf(tds_req,"t%d.txt=\"%.2f\"%s\0",6,tds,end_char);
	sprintf(chemical1_req,"t%d.txt=\"%.2f\"%s\0",8,chemical1,end_char);
	sprintf(chemical2_req,"t%d.txt=\"%.2f\"%s\0",11,chemical2,end_char);

	sprintf(data,"%s%s%s%s%s\0",total_water_req,ph_req,tds_req,chemical1_req,chemical2_req);
	HAL_UART_Transmit_IT(&huart5,&data,strlen(data));

}

void get_settings(char *request){
	char data[200];
	sprintf(water_meter_req,"t%d.txt=\"%u\"%s\0",2,water_settings.water_meter_settings,end_char);
	sprintf(chemical1_req,"t%d.txt=\"%.2f\"%s\0",5,water_settings.chemical1_settings,end_char);
	sprintf(chemical2_req,"t%d.txt=\"%.2f\"%s\0",8,water_settings.chemical2_settings,end_char);

	sprintf(data,"%s%s%s%s\0",request,water_meter_req,chemical1_req,chemical2_req);
	HAL_UART_Transmit_IT(&huart5,&data,strlen(data));
}
