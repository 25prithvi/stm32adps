/* USER CODE BEGIN Header */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "flash_storage.h"


void get_settings_flash(settings *flash_water_settings){
	flash_water_settings->water_meter_settings = FLASH_READ(water_meter_setting_address);

}

void set_settings_flash(settings *flash_water_settings){
	HAL_FLASH_Unlock();

	FLASH_Program_Word(water_meter_setting_address, flash_water_settings->water_meter_settings);

	HAL_Delay(2);

	HAL_FLASH_Lock();
}


static void FLASH_Program_Word(uint32_t Address, uint32_t Data){
  /* Check the parameters */
  assert_param(IS_FLASH_ADDRESS(Address));

  FLASH_Erase_Sector(FLASH_SECTOR_23,FLASH_VOLTAGE_RANGE_3);

  HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, Address, Data);
}

static uint32_t FLASH_READ(uint32_t Flash_address){
	uint32_t Flash_data;

	Flash_data = *(uint32_t*) Flash_address;

	return Flash_data;

}
