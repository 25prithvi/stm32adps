/* USER CODE BEGIN Header */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "keys.h"
#include "adps_screen.h"
#include "keypad_screen.h"

UART_HandleTypeDef huart5;

TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;

char keypad_command[50];
char edit_data[15];
int edit_data_len;

int keypad_state = KEY_1;

void keypad_action(){

	switch(keypad_state){

	case KEY_1:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(1,210);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_OK;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(1,2);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_2;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(1);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len<12)
			{
				presskey(1);
				edit_data[edit_data_len] = '1';
				edit_data[edit_data_len + 1] = '\0';
				edit_data_len++;
				key_pressed = NONE_KEY_PRESSED;
			}
		}
		break;

	case KEY_2:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(2,1);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(2,3);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_3;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(2);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len<12)
			{
				presskey(2);
				edit_data[edit_data_len] = '2';
				edit_data[edit_data_len + 1] = '\0';
				edit_data_len++;
				key_pressed = NONE_KEY_PRESSED;
			}
		}
		break;

	case KEY_3:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(3,2);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_2;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(3,4);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_4;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(3);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len<12)
			{
				presskey(3);
				edit_data[edit_data_len] = '3';
				edit_data[edit_data_len + 1] = '\0';
				edit_data_len++;
				key_pressed = NONE_KEY_PRESSED;
			}
		}
		break;

	case KEY_4:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(4,3);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_3;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(4,5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_5;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(4);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len<12)
			{
				presskey(4);
				edit_data[edit_data_len] = '4';
				edit_data[edit_data_len + 1] = '\0';
				edit_data_len++;
				key_pressed = NONE_KEY_PRESSED;
			}
		}
		break;

	case KEY_5:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(5,4);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_4;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(5,6);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_6;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(5);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len<12)
			{
				presskey(5);
				edit_data[edit_data_len] = '5';
				edit_data[edit_data_len + 1] = '\0';
				edit_data_len++;
				key_pressed = NONE_KEY_PRESSED;
			}
		}
		break;

	case KEY_6:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(6,5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_5;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(6,7);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_7;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(6);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len<12)
			{
				presskey(6);
				edit_data[edit_data_len] = '6';
				edit_data[edit_data_len + 1] = '\0';
				edit_data_len++;
				key_pressed = NONE_KEY_PRESSED;
			}
		}
		break;

	case KEY_7:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(7,6);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_6;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(7,8);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_8;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(7);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len<12)
			{
				presskey(7);
				edit_data[edit_data_len] = '7';
				edit_data[edit_data_len + 1] = '\0';
				edit_data_len++;
				key_pressed = NONE_KEY_PRESSED;
			}
		}
		break;

	case KEY_8:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(8,7);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_7;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(8,9);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_9;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(8);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len<12)
			{
				presskey(8);
				edit_data[edit_data_len] = '8';
				edit_data[edit_data_len + 1] = '\0';
				edit_data_len++;
				key_pressed = NONE_KEY_PRESSED;
			}
		}
		break;

	case KEY_9:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(9,8);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_8;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(9,0);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_0;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(9);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len<12)
			{
				presskey(9);
				edit_data[edit_data_len] = '9';
				edit_data[edit_data_len + 1] = '\0';
				edit_data_len++;
				key_pressed = NONE_KEY_PRESSED;
			}
		}
		break;

	case KEY_0:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(0,9);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_9;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED && current_screen_state == EDIT_WATER_METER_SCREEN)
		{
			goto_key(0,200);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_DEL;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(0,99);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_POINT;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(0);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len<12)
			{
				presskey(0);
				edit_data[edit_data_len] = '0';
				edit_data[edit_data_len + 1] = '\0';
				edit_data_len++;
				key_pressed = NONE_KEY_PRESSED;
			}
		}
		break;

	case KEY_POINT:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(99,0);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_0;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(99,200);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_DEL;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(99);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len<12)
			{
				presskey(99);
				edit_data[edit_data_len] = '.';
				edit_data[edit_data_len + 1] = '\0';
				edit_data_len++;
				key_pressed = NONE_KEY_PRESSED;
			}
		}
		break;

	case KEY_DEL:
		if(key_pressed == LEFT_KEY_PRESSED && current_screen_state == EDIT_WATER_METER_SCREEN)
		{
			goto_key(200,0);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_0;
		}
		else if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(200,99);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_POINT;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(200,210);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_OK;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(200);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(edit_data_len>0)
			{
				presskey(200);
				edit_data_len--;
				edit_data[edit_data_len] = '\0';
				key_pressed = NONE_KEY_PRESSED;
			}

		}
		break;

	case KEY_OK:
		if(key_pressed == LEFT_KEY_PRESSED)
		{
			goto_key(210,200);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_DEL;
		}
		else if(key_pressed == RIGHT_KEY_PRESSED)
		{
			goto_key(210,1);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == ESC_KEY_PRESSED)
		{
			close_keypad(210);
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
		}
		else if(key_pressed == MENU_KEY_PRESSED)
		{
			if(current_screen_state == EDIT_WATER_METER_SCREEN)
			{
				if(water_settings.water_meter_settings != atoi(edit_data)){
					water_settings.water_meter_settings = atoi(edit_data);
					settings_changed_flag = 1;
				}

			}
			else if(current_screen_state == EDIT_CHEMICAL1_SCREEN)
			{
				if(water_settings.chemical1_settings != charToFloat()){
					water_settings.chemical1_settings = charToFloat();
					settings_changed_flag = 1;
				}
			}
			else if(current_screen_state == EDIT_CHEMICAL2_SCREEN)
			{
				if(water_settings.chemical2_settings != charToFloat()){
					water_settings.chemical2_settings = charToFloat();
					settings_changed_flag = 1;
				}
			}
			sprintf(keypad_command,"b210.bco=14823%sclick b210,0%s\0",end_char,end_char);
			HAL_UART_Transmit_IT(&huart5,keypad_command,strlen(keypad_command));
			key_pressed = NONE_KEY_PRESSED;
			keypad_state = KEY_1;
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_Base_Stop_IT(&htim5);
		}
		break;

	}
}

void goto_key(int from_key, int to_key){
	sprintf(keypad_command,"b%d.bco=14823%sb%d.bco=0%s\0",from_key,end_char,to_key,end_char);
	HAL_UART_Transmit_IT(&huart5,keypad_command,strlen(keypad_command));
}

void close_keypad(int key){
	sprintf(keypad_command,"b%d.bco=14823%sclick b251,0%s\0",key,end_char,end_char);
	HAL_UART_Transmit_IT(&huart5,keypad_command,strlen(keypad_command));
}

void presskey(int key){
	sprintf(keypad_command,"click b%d,0%s\0",key,end_char);
    HAL_UART_Transmit_IT(&huart5,keypad_command,strlen(keypad_command));
}

float charToFloat(){
	char decimal_part_char[3];
	int pointFlag = 0;
	int i = 0;
	int intPart;
	int decimalPart;
	while(edit_data[i] != '\0')
	{
		if(edit_data[i] == '.')
			pointFlag = 1;
		else if(pointFlag >= 1)
			{
				decimal_part_char[pointFlag - 1] = edit_data[i];
				pointFlag++;
			}
		if(pointFlag == 3)
		{
			break;
		}
		i++;
	}
	decimal_part_char[pointFlag] = '\0';

	intPart = atoi(edit_data);
	decimalPart = atoi(decimal_part_char);
	return ((float)intPart + ((float)decimalPart/100));
}
