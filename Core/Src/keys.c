/* USER CODE BEGIN Header */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "keys.h"


int key_pressed_flag;
int key_pressed = NONE_KEY_PRESSED;
UART_HandleTypeDef huart5;

uint32_t LEFT_KEY = GPIO_PIN_1;
uint32_t RIGHT_KEY = GPIO_PIN_3;
uint32_t MENU_KEY = GPIO_PIN_4;
uint32_t ESC_KEY = GPIO_PIN_6;




void keys_action(){

	if(HAL_GPIO_ReadPin(GPIOD,LEFT_KEY) == 1 && key_pressed_flag == NONE_KEY_PRESSED )
	{
		key_pressed_flag = LEFT_KEY_PRESSED;
	}
	else if(HAL_GPIO_ReadPin(GPIOD,LEFT_KEY) == 0 && key_pressed_flag == LEFT_KEY_PRESSED)
	{

		key_pressed = LEFT_KEY_PRESSED;
		key_pressed_flag = NONE_KEY_PRESSED;
	}
	else if(HAL_GPIO_ReadPin(GPIOD,RIGHT_KEY) == 1 && key_pressed_flag == NONE_KEY_PRESSED )
	{
		key_pressed_flag = RIGHT_KEY_PRESSED;
	}
	else if(HAL_GPIO_ReadPin(GPIOD,RIGHT_KEY) == 0 && key_pressed_flag == RIGHT_KEY_PRESSED)
	{
		key_pressed = RIGHT_KEY_PRESSED;
		key_pressed_flag = NONE_KEY_PRESSED;
	}
	else if(HAL_GPIO_ReadPin(GPIOD,MENU_KEY) == 1 && key_pressed_flag == NONE_KEY_PRESSED )
	{
		key_pressed_flag = MENU_KEY_PRESSED;
	}
	else if(HAL_GPIO_ReadPin(GPIOD,MENU_KEY) == 0 && key_pressed_flag == MENU_KEY_PRESSED)
	{
		key_pressed = MENU_KEY_PRESSED;
		key_pressed_flag = NONE_KEY_PRESSED;
	}
	else if(HAL_GPIO_ReadPin(GPIOD,ESC_KEY) == 1 && key_pressed_flag == NONE_KEY_PRESSED )
	{
		key_pressed_flag = ESC_KEY_PRESSED;
	}
	else if(HAL_GPIO_ReadPin(GPIOD,ESC_KEY) == 0 && key_pressed_flag == ESC_KEY_PRESSED)
	{
		key_pressed = ESC_KEY_PRESSED;
		key_pressed_flag = NONE_KEY_PRESSED;
	}

}
